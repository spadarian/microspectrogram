# Microspectrogram

A simple `no_std` library for computing spectrograms.

![Example visualisation of a spectrogram](assets/microspectrogram.png "Example visualisation of a spectrogram")

## Implementation details

* The FFT is performed by the [microfft](https://docs.rs/microfft/latest/microfft/) create. It uses the Radix-2 FFT algorithm so the number of samples is limited to powers of 2.
* It follows the [spectrogram implementation](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html) of the scipy python library.
* This library implements the minimum requirements for one of my projects so many features are missing (e.g. windows, scaling functions, detrending function, etc). At the moment, it is equivalent to the default parameters of the scipy implementation. Merge requests are welcome.

## Usage

See [examples/spectrogram.rs](examples/spectrogram.rs) for the complete example that generates the spectrogram image above. 
